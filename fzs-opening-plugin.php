<?php
/*
Plugin Name: FZS Job Opening Plugin
Plugin URI: http://the406.com
Description: A simple add-on to support world domination!
Version: 1.0.1
Author: Bradford Knowlton
Author URI: http://www.bradknowlton.com
Text Domain: fzs
Domain Path: /languages

------------------------------------------------------------------------
Copyright 2012-2017 Bradford Knowlton

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
*
* Changelog
* 
* 1.0.1 Initial Version
*
**/

define( 'FZS_OPENING_VERSION', '1.0.1' );

define( 'FZS_OPENING_PATH', plugin_dir_path( __FILE__ ) );

class FZS_Opening{
	
	public function __construct()
    {
    	add_action( 'init', array( $this, 'register_opening' ) );
    }
	
	function register_opening() {
	    register_post_type( 'opening',
	        array(
	            'labels' => array(
	                'name' => __('Job Openings', 'fzs'),
	                'singular_name' => __('Job Opening', 'fzs'),
	                'add_new' => __('Add New', 'fzs'),
	                'add_new_item' => __('Add New Job Opening', 'fzs'),
	                'edit' => __('Edit', 'fzs'),
	                'edit_item' => __('Edit Opening', 'fzs'),
	                'new_item' => __('New Job Opening', 'fzs'),
	                'view' => __('View', 'fzs'),
	                'view_item' => __('View Opening', 'fzs'),
	                'search_items' => __('Search Openings', 'fzs'),
	                'not_found' => __('No Openings found', 'fzs'),
	                'not_found_in_trash' => __('No Opening found in trash', 'fzs'),
	                'parent' => __('Parent Opening', 'fzs')
	            ),
	 
	            'public' => true,
	            'menu_position' => 15,
	            'supports' => array( 'title', 'editor', 'thumbnail' ), //  'comments', 'custom-fields'
	            'taxonomies' => array( '' ),
	            'menu_icon' => 'dashicons-clipboard',
	            'has_archive' => true
	        )
	    );
	    
	}
	
}

$fzs_opening = new FZS_Opening();


function business_query( $args, $field, $post ) {
    if( current_user_can('editor') || current_user_can('administrator') ) {
    	return $args;
    }
    
    // limit posts for current logged in user
    $args['author'] = get_current_user_id();
    
    return $args;
}
add_filter('acf/fields/post_object/query/name=business', 'business_query', 10, 3);
